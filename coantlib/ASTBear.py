"""ASTBear module."""

from coantlib.ASTWalker import ASTWalker
from coalib.bears.LocalBear import LocalBear


class ASTBear(LocalBear):
    """
    All coantbears must inherit this bear.

    Every inherited bear is responsible for calling the run method of this
    bear, so that class variable ``walker`` is set and availaible for the
    traversal of parse-tree.
    """

    def run(self,
            filename,
            file):
        """
        Set the appropriate walker for a ``coantbear``.

        Auto-detects the language depending on the class variable set.
        """
        if not self.LANGUAGES:
            lang = filename.split('.')[-1]
        else:
            lang = list(self.LANGUAGES)[0]
        self.walker = ASTWalker.get_walker(lang)(filename, file, lang)
