import unittest

from queue import Queue

from coantbears.XML.XMLIndentBear import XMLIndentBear
from coalib.testing.LocalBearTestHelper import verify_local_bear
from coalib.settings.Section import Section

xml_file_s1_good = """<?xml version="1.0" encoding="UTF-8"?>
<feature id="com.coala.feature"
    label="Feature"
    version="1.0.0.qualifier"
    provider-name="coala">

    <description url="http://coala-analyzer.org/">
      The coala Eclipse plug-in provides static code analysis using coala.
Visit http://coala-analyzer.org/ for more information.
    </description>

    <license url="https://www.gnu.org/licenses/agpl.txt">
      This program and the accompanying materials are made available under the
      terms of the AGPL License v3.
    </license>

    <plugin id="com.coala.core"
        download-size="0"
        install-size="0"
        version="0.0.0"
        unpack="false"/>
    <tag att="1"
        att2="2"
        att3="3"/>

    <good in="one line"> These tests are crazy ! </good>
    <noattr> No attribute </noattr>

</feature>
"""


xml_file_s2_good = """<?xml version="1.0" encoding="UTF-8"?>
<feature
    id="com.coala.feature"
    label="Feature"
    version="1.0.0.qualifier"
    provider-name="coala"
>

    <description
        url="http://coala-analyzer.org/">
      The coala Eclipse plug-in provides static code analysis using coala.
Visit http://coala-analyzer.org/ for more information.
    </description>

    <license
        url="https://www.gnu.org/licenses/agpl.txt"
    >
      This program and the accompanying materials are made available under the
      terms of the AGPL License v3.
    </license>

    <plugin
        id="com.coala.core"
        download-size="0"
        install-size="0"
        version="0.0.0"
        unpack="false"
    />
    <tag
        att="1"
        att2="2"
        att3="3"
    />

    <test
    />

    <test2></test2>

</feature>
"""

xml_file_s2_bad = """<?xml version="1.0" encoding="UTF-8"?>
<feature id="com.coala.feature"
    label="Feature"
    version="1.0.0.qualifier"
    provider-name="coala">

    <description url="http://coala-analyzer.org/">
      The coala Eclipse plug-in provides static code analysis using coala.
Visit http://coala-analyzer.org/ for more information.
    </description>

    <license url="https://www.gnu.org/licenses/agpl.txt">
      This program and the accompanying materials are made available under the
      terms of the AGPL License v3.
    </license>

    <plugin id="com.coala.core"
        download-size="0"
        install-size="0"
        version="0.0.0"
        unpack="false"/>
    <tag att="1"
        att2="2"
        att3="3"/>

</feature>
"""


xml_file_s1_bad = """<?xml version="1.0" encoding="UTF-8"?>
<feature
      id="com.coala.feature"
      label="Feature"
      version="1.0.0.qualifier"
      provider-name="coala">

   <description url="http://coala-analyzer.org/">
      The coala Eclipse plug-in provides static code analysis using coala.
Visit http://coala-analyzer.org/ for more information.
   </description>

   <license url="https://www.gnu.org/licenses/agpl.txt">
      This program and the accompanying materials are made available under the
      terms of the AGPL License v3.
   </license>

   <plugin
         id="com.coala.core"
         download-size="0"
         install-size="0"
         version="0.0.0"
         unpack="false"/>
   <tag att="1" att2="2" att3="3"/>
      <bad in="one line"> These tests are crazy ! </bad>

</feature
>
"""

xml_file_bad = """<?xml version="1.0" encoding="UTF-8"?>
<feature
      id="com.coala.feature"
      label="Feature"
      version="1.0.0.qualifier"
      provider-name="coala">

   <description url="http://coala-analyzer.org/">
      The coala Eclipse plug-in provides static code analysis using coala.
Visit http://coala-analyzer.org/ for more information.
   </description>

   <license url="https://www.gnu.org/licenses/agpl.txt">
      This program and the accompanying materials are made available under the
      terms of the AGPL License v3.
</license>

   <plugin
         id="com.coala.core"
         download-size="0"
         install-size="0"
         version="0.0.0"
         unpack="false"/>

         <test tag="t"> Hola ! </test>

         <makes no="sense" but="still" it="is here"
/>
<simple/>
    </feature>
"""

one_liner = """<?xml version="1.0" encoding="UTF-8"?>
<body>
    <feature id="one line definition"> Text content for one liner </feature>
    <feature2> Text content for this one liner </feature2>
    <good attr="1"></good>
</body>
"""

style_2_testing = """
<wow
    att="1"
/>
"""

style_2_bad_2 = """
<body>
  <whee/>
  <feature>oneline</feature>
    <feature2>oneline two</feature2>
</body>
"""

style_2_bad_one_line = """
<body>
    <oneline attr="1"/>
</body>
"""

any_style_bad_one_line = """
<body>
            <oneline attr="1"/>
</body>
"""
style1 = verify_local_bear(
    XMLIndentBear,
    valid_files=(xml_file_s1_good, one_liner),
    invalid_files=(xml_file_bad, xml_file_s1_bad, xml_file_s2_good,
                   any_style_bad_one_line,),
    )

style2 = verify_local_bear(
    XMLIndentBear,
    valid_files=(xml_file_s2_good, one_liner),
    invalid_files=(xml_file_s2_bad, xml_file_s1_good,
                   style_2_bad_2, xml_file_bad, style_2_bad_one_line,
                   any_style_bad_one_line),
    settings={'style': 2},
    )


class InvalidArgsTest(unittest.TestCase):
    def setUp(self):
        self.msg_queue = Queue()
        self.section = Section('')
        self.uut = XMLIndentBear(self.section, self.msg_queue)

    def test_invalid_style(self):
        with self.assertRaises(AssertionError, msg='Invalid style 3.'):
            y = list(self.uut.run('testfile.xml', xml_file_bad, style=3))
